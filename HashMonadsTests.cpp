#define CATCH_CONFIG_MAIN
#include "HashTableMonad.hpp"
#include <algorithm>
#include <catch2/catch.hpp>
#include <random>
#include <set>
#include <tuple>
#include <vector>

using Aggregation = HashTableMonad<int, int>::Aggregation;
using std::mt19937;
using std::pair;
using std::random_device;
using std::set;
using std::tuple;
using std::vector;
namespace mocks {

auto getRange(size_t range = 32) {
  vector<int> input;
  for(size_t i = 0; i < range; i++)
    input.push_back(i);
  return input;
}

auto getShuffledRange(size_t range = 32) {
  auto input = getRange(range);
  std::shuffle(input.begin(), input.end(), mt19937(random_device()()));
  return input;
}

} // namespace mocks

TEST_CASE("simple hash aggregation works", "[building]") {
  HashTableMonad<int, int> table({.size = 4, .aggregation = Aggregation::Sum});
  for(size_t i = 0; i < 16; i++)
    table.insert({i % 4, 1});
  for(size_t i = 0; i < table.size(); i++)
    REQUIRE(table.at(i).value() == 4);
}

TEST_CASE("hash aggregation with conflicts works", "[building]") {
  HashTableMonad<int, int> table({.size = 4, .aggregation = Aggregation::Sum});
  for(size_t i = 0; i < 16; i++)
    table.insert({(i % 4) * 2, 1});
  for(size_t i = 0; i < table.size(); i++)
    REQUIRE(table.at((i % 4) * 2).value() == 4);
}

TEST_CASE("hash build without aggregation or conflicts", "[building]") {
  HashTableMonad<int, int> table({.size = 32, .aggregation = Aggregation::None});
  for(size_t i = 0; i < 16; i++)
    table.insert({i * 2, i});
  auto count = 0ul;
  for(size_t i = 0; i < table.size(); i++)
    count += (table.getBuffer().at(i).marker == 1);
  REQUIRE(count == 16);
}

TEST_CASE("hash build without aggregation with conflicts", "[building]") {
  HashTableMonad<int, int> table({.size = 32, .aggregation = Aggregation::None});
  for(size_t i = 0; i < 16; i++)
    table.insert({(i % 4) * 2, i});
  auto count = 0ul;
  for(size_t i = 0; i < table.size(); i++)
    count += (table.getBuffer().at(i).marker == 1);
  REQUIRE(count == 16);
}

TEST_CASE("hash build without aggregation from vector", "[building]") {
  vector<int> input;

  for(size_t i = 0; i < 32; i++)
    input.push_back(i);

  std::shuffle(input.begin(), input.end(), mt19937(random_device()()));

  HashTableMonad<int, int> table({.size = input.size() * 2ul});
  for(size_t i = 0; i < input.size(); i++)
    table.insert({input[i], i});
  for(auto& value : input) {
    REQUIRE(table.at(value).has_value());
  }
}

TEST_CASE("bulk hash build without aggregation from vector", "[building]") {
  auto input = mocks::getShuffledRange();
  auto positions = mocks::getRange();

  HashTableMonad<int, int> table({.size = input.size() * 2ul});
  table.insert(input, positions);
  for(auto& value : input)
    REQUIRE(table.at(value).has_value());
}

TEST_CASE("hash probing", "[probing]") {
  auto buildInput = mocks::getShuffledRange();
  auto probeInput = mocks::getShuffledRange();

  auto positions = mocks::getRange();
  HashTableMonad<int, int> hashTable({.size = buildInput.size() * 2ul});
  hashTable.insert(buildInput, positions);

  vector<pair<int, int>> output;
  for(size_t i = 0; i < probeInput.size(); i++)
    if(hashTable.at(probeInput.at(i)).has_value())
      output.emplace_back(hashTable.at(probeInput.at(i)).value(), positions.at(i));
  REQUIRE(output.size() == probeInput.size());

  set<pair<int, int>> outputSet;
  outputSet.insert(output.begin(), output.end());
  for(size_t i = 0; i < buildInput.size(); i++)
    for(size_t j = 0; j < probeInput.size(); j++)
      if(buildInput.at(i) == probeInput.at(j))
        REQUIRE(outputSet.count({i, j}) > 0);
}

TEST_CASE("bulk hash probing", "[probing]") {
  auto buildInput = mocks::getShuffledRange();
  auto probeInput = mocks::getShuffledRange();

  auto positions = mocks::getRange();
  HashTableMonad<int, int> hashTable({.size = buildInput.size() * 2ul});
  hashTable.insert(buildInput, positions);

  vector<pair<int, int>> output = hashTable.at(probeInput, positions);
  REQUIRE(output.size() == probeInput.size());

  set<pair<int, int>> outputSet;
  outputSet.insert(output.begin(), output.end());
  for(size_t i = 0; i < buildInput.size(); i++)
    for(size_t j = 0; j < probeInput.size(); j++)
      if(buildInput.at(i) == probeInput.at(j))
        REQUIRE(outputSet.count({i, j}) > 0);
}

TEST_CASE("multi-way hash probing", "[probing]") {
  auto buildInput1 = mocks::getShuffledRange();
  auto buildInput2 = mocks::getShuffledRange();
  auto probeInput = mocks::getShuffledRange();

  auto positions = mocks::getRange();
  HashTableMonad<int, int> hashTable({.size = buildInput1.size() * 2ul});
  hashTable.insert(buildInput1, positions);
  HashTableMonad<int, int> hashTable2({.size = buildInput1.size() * 2ul});
  hashTable2.insert(buildInput2, positions);

  vector<tuple<int, int, int>> output;
  for(size_t i = 0; i < probeInput.size(); i++)
    if(hashTable.at(probeInput.at(i)).has_value())
      if(hashTable2.at(probeInput.at(i)).has_value())
        output.emplace_back(hashTable.at(probeInput.at(i)).value(),
                            hashTable2.at(probeInput.at(i)).value(), positions.at(i));
  REQUIRE(output.size() == probeInput.size());

  set<tuple<int, int, int>> outputSet;
  outputSet.insert(output.begin(), output.end());
  for(size_t j = 0; j < probeInput.size(); j++)
    for(size_t i = 0; i < buildInput1.size(); i++)
      if(buildInput1.at(i) == probeInput.at(j))
        for(size_t k = 0; k < buildInput1.size(); k++)
          if(buildInput2.at(k) == probeInput.at(j))
            REQUIRE(outputSet.count({i, k, j}) > 0);
}

TEST_CASE("bulk multi-way hash probing", "[probing]") {
  auto buildInput1 = mocks::getShuffledRange(64);
  auto positions1 = mocks::getRange(64);
  auto buildInput2 = mocks::getShuffledRange();
  auto probeInput = mocks::getShuffledRange();

  auto positions = mocks::getRange();
  HashTableMonad<int, int> hashTable({.size = buildInput1.size() * 2ul});
  hashTable.insert(buildInput1, positions1);
  HashTableMonad<int, int> hashTable2({.size = buildInput1.size() * 2ul});
  hashTable2.insert(buildInput2, positions);

  auto output = hashTable.chain(hashTable2).at<int, int, int>(probeInput, positions);

  REQUIRE(output.size() == probeInput.size());

  set<tuple<int, int, int>> outputSet;
  outputSet.insert(output.begin(), output.end());
  for(size_t k = 0; k < probeInput.size(); k++)
    for(size_t i = 0; i < buildInput1.size(); i++)
      if(buildInput1.at(i) == probeInput.at(k))
        for(size_t j = 0; j < buildInput2.size(); j++)
          if(buildInput2.at(j) == probeInput.at(k))
            REQUIRE(outputSet.count({i, j, k}) > 0);
}
