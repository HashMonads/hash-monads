#ifndef PROBE_H
#define PROBE_H
#include <utility>
#define LLVM_DISABLE_ABI_BREAKING_CHECKS_ENFORCING 1
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#include <codegen/arithmetic_ops.hpp>
#include <codegen/compiler.hpp>
#include <codegen/module.hpp>
#include <codegen/relational_ops.hpp>
#include <codegen/statements.hpp>
#include <codegen/variable.hpp>
#pragma clang diagnostic pop
static codegen::module_builder* builderPointer; // TODO: Fix this terrible hack

template <typename HashTable, typename ProbeValue,
          typename ResultTypeParameter = std::pair<typename HashTable::ValueType, ProbeValue>>
struct Probe {
  using ResultType = ResultTypeParameter;
  struct Internals {
    codegen::module&& module;
    std::function<size_t(char*, size_t, char**, unsigned int*, typename HashTable::KeyType*,
                         ProbeValue*, size_t)>
        performProbe;
  };
  Internals internals;

  static void prepareFindCandidateSlotFunctionImplementation(codegen::module_builder& b) {
    builderPointer = &b;
  }

  static auto createFindCandidateSlotFunctionImplementation() {
    auto& builder = *builderPointer;
    namespace cg = codegen;
    return builder.create_function<unsigned int(char*, unsigned int, typename HashTable::KeyType)>(
        "findCandiateslot",
        [&](cg::value<char*> hashTable, cg::value<unsigned int> hashTableSizeInEntries,
            cg::value<typename HashTable::KeyType> key) {
          auto crc = cg::call(HashTable::crcFunction(*builder.context_), cg::constant(0u),
                              cg::bit_cast<unsigned int>(key));
          auto slotPosition = cg::variable<unsigned int>("slot", crc % hashTableSizeInEntries);
          typename HashTable::Slot slot{hashTable, slotPosition};
          cg::while_(
              [&] {
                return cg::value<bool>{((cg::load((slot.keyAddress())) != key) &
                                        (cg::load((slot.markerAddress())) !=
                                         cg::constant<decltype(HashTable::Entry::marker)>(0)))
                                           .eval(),
                                       ""};
              },
              [&] { slot.set((slot.get() + cg::constant(1u)) % hashTableSizeInEntries); });
          cg::return_(slot.get());
        });
  }

  static void processSlotImplementation(
      codegen::value<char**> pointerToHashTable, codegen::value<unsigned int*>,
      codegen::variable<unsigned int>& position, codegen::value<typename HashTable::KeyType>,
      codegen::variable<char*>& untypedHashValueOutputSlot,
      std::function<void(codegen::variable<char*>&)> processNextValue) {
    namespace cg = codegen;
    typename HashTable::Slot slot{cg::load(pointerToHashTable), position};

    auto hashValueOutputSlot =
        cg::bit_cast<typename HashTable::ValueType*>(untypedHashValueOutputSlot.get());
    cg::if_(cg::load(slot.markerAddress()) > cg::constant<decltype(HashTable::Entry::marker)>(0),
            [&] {
              cg::store(cg::load(slot.valueAddress()), hashValueOutputSlot);
              untypedHashValueOutputSlot.set(untypedHashValueOutputSlot.get() +
                                             cg::constant(sizeof(typename HashTable::ValueType)));
              processNextValue(untypedHashValueOutputSlot);
            });
  };

  Probe(codegen::compiler& compiler,
        std::function<void(codegen::module_builder&)> prepareFindCandidateSlotFunction =
            prepareFindCandidateSlotFunctionImplementation,
        std::function<
            codegen::function_ref<unsigned int, char*, unsigned int, typename HashTable::KeyType>()>
            createFindCandidateSlotFunction = createFindCandidateSlotFunctionImplementation,
        std::function<void(codegen::value<char**>, codegen::value<unsigned int*>,
                           codegen::variable<unsigned int>&,
                           codegen::value<typename HashTable::KeyType>, codegen::variable<char*>&,
                           std::function<void(codegen::variable<char*>&)>)>
            processSlot = processSlotImplementation)
      : internals([&compiler, processSlot, createFindCandidateSlotFunction,
                   prepareFindCandidateSlotFunction]() -> Internals {
          namespace cg = codegen;

          auto builder = cg::module_builder(compiler, "module_name");

          prepareFindCandidateSlotFunction(builder);
          auto findCandidateSlot = createFindCandidateSlotFunction();

          auto probeFunctionRef =
              builder.create_function<size_t(char*, size_t, char**, unsigned int*,
                                             typename HashTable::KeyType*, ProbeValue*, size_t)>(
                  "probe",
                  [&](cg::value<char*> outputBuffer,
                      cg::value<size_t> /*outputBufferSizeInEntries*/, cg::value<char**> hashTables,
                      cg::value<unsigned int*> hashTableSizesInEntries,
                      cg::value<typename HashTable::KeyType*> probeKeys,
                      cg::value<ProbeValue*> probeValues,
                      cg::value<size_t> probeBufferSizeInEtries) {
                    auto probeIterator = cg::variable<size_t>("probeIterator", cg::constant(0ul));
                    auto outputIterator = cg::variable<size_t>("outputIterator", cg::constant(0ul));
                    auto processProbeValue = [&probeIterator, &probeValues, &outputIterator](
                                                 cg::variable<char*>& untypedHashValueOutputSlot) {
                      cg::store(cg::load(probeValues + probeIterator.get()),
                                cg::bit_cast<ProbeValue*>(untypedHashValueOutputSlot.get()));
                      outputIterator.set(outputIterator.get() + cg::constant(sizeof(ResultType)));
                    };
                    cg::while_(
                        [&] { return probeIterator.get() < probeBufferSizeInEtries; },
                        [&] {
                          cg::variable<char*> columnOffset{"columnOffset",
                                                           outputBuffer + outputIterator.get()};
                          auto hashTableSlotPosition = cg::variable<unsigned int>(
                              "candidateSlot", cg::call(findCandidateSlot, cg::load(hashTables),
                                                        cg::load(hashTableSizesInEntries),
                                                        cg::load(probeKeys + probeIterator.get())));
                          auto key = cg::load(probeKeys + probeIterator.get());
                          processSlot(hashTables, hashTableSizesInEntries, hashTableSlotPosition,
                                      key, columnOffset, processProbeValue);
                          probeIterator.set(probeIterator.get() + cg::constant(1ul));
                        });

                    codegen::return_(outputIterator.get());
                  });
          auto module = std::move(builder).build();
          auto probeFunction = module.get_address(probeFunctionRef);
          return {std::move(module), probeFunction};
        }()) {}
};

#endif /* PROBE_H */
