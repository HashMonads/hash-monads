#define LLVM_DISABLE_ABI_BREAKING_CHECKS_ENFORCING 1
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#include <codegen/arithmetic_ops.hpp>
#include <codegen/compiler.hpp>
#include <codegen/module.hpp>
#include <codegen/relational_ops.hpp>
#include <codegen/statements.hpp>
#include <codegen/variable.hpp>
#pragma clang diagnostic pop

#include <llvm/IR/Function.h>
#include <nmmintrin.h>
#include <optional>
#include <type_traits>
#include <utility>
#include <vector>

#include "Probe.hpp"
#include "ProbeChain.hpp"

template <typename Key, typename Value> class HashTableMonad {
public:
  typedef Key KeyType;
  typedef Value ValueType;
  enum class Aggregation { None, Min, Max, Sum, Count };
  struct Entry {
    int marker;
    Key key;
    Value value;
  };

private:
  std::vector<Entry> buffer;
  codegen::compiler compiler;
  struct Internals {
    codegen::module const&& m; // the module seems to own the function pointers
    std::function<void(char*, unsigned int, Key, Value)> const insert;
    std::function<void(char*, unsigned int, Key*, Value*, unsigned int)> const bulkInsert;
  };
  Internals const internal{};

public:
  struct Slot {
    codegen::value<char*> state;
    codegen::variable<unsigned int>& position;

    auto slotAddress() {
      return state + (position.get() * codegen::constant((unsigned int)sizeof(Entry)));
    }
    auto markerAddress() { return codegen::bit_cast<decltype(Entry::marker)*>(slotAddress()); };
    auto keyAddress() {
      return codegen::bit_cast<decltype(Entry::key)*>(
          slotAddress() + codegen::constant<int>(sizeof(Entry::marker)));
    };
    auto valueAddress() {
      return codegen::bit_cast<decltype(Entry::value)*>(
          slotAddress() +
          codegen::constant<unsigned int>(sizeof(Entry::marker) + sizeof(Entry::key)));
    };

    template <typename SlotValue> void set(SlotValue t) { position.set(t); }
    auto get() { return position.get(); }
  };

  static auto crcFunction(llvm::LLVMContext& context) {
    return codegen::function_ref<uint, uint, uint>{
        "crc32", llvm::Function::Create(
                     llvm::FunctionType::get(
                         llvm::Type::getInt32Ty(context),
                         {llvm::Type::getInt32Ty(context), llvm::Type::getInt32Ty(context)}, false),
                     llvm::Function::ExternalLinkage, "llvm.x86.sse42.crc32.32.32")};
  }

  template <typename HashKey, std::enable_if_t<std::is_fundamental<HashKey>::value, int> = 0>
  auto calculateCRC(codegen::module_builder& builder, codegen::value<HashKey>& key) {
    namespace cg = codegen;
    return cg::call(crcFunction(*builder.context_), cg::constant(0u),
                    cg::bit_cast<unsigned int>(key));
  }
  struct Configuration {
    size_t size;
    Aggregation aggregation = Aggregation::None;
  };
  void insert(std::pair<Key, Value> newEntry) {
    internal.insert((char*)buffer.data(), buffer.size(), newEntry.first, newEntry.second);
  };
  void insert(std::vector<Key> keys, std::vector<Value> values) {
    internal.bulkInsert((char*)buffer.data(), buffer.size(), keys.data(), values.data(),
                        keys.size());
  };
  auto& getBuffer() const { return buffer; }
  auto size() const { return buffer.size(); };
  std::optional<Value> at(Key i) {
    auto crc = _mm_crc32_u32(i, 0) % buffer.size();
    auto remainingSlotsToLookAt = buffer.size();
    while(buffer.at(crc).marker && remainingSlotsToLookAt > 0) {
      if(buffer.at(crc).key == i)
        return buffer.at(crc).value;
      crc = (crc + 1) % buffer.size();
      remainingSlotsToLookAt--;
    }
    return {};
  };

  template <typename ProbeValue>
  auto at(std::vector<Key> probeKeys, std::vector<ProbeValue> probeValues,
          std::optional<size_t> providedSizeEstimate = {}) {

    auto sizeEstimate = providedSizeEstimate.value_or(probeKeys.size() * 2);
    Probe<HashTableMonad<Key, Value>, ProbeValue> probe(compiler);
    std::vector<typename decltype(probe)::ResultType> output;
    output.resize(sizeEstimate);
    unsigned int sizes[]{(unsigned int)buffer.size()};
    char* buffers[]{(char*)buffer.data()};
    output.resize(probe.internals.performProbe((char*)output.data(), output.size(), buffers, sizes,
                                               probeKeys.data(), probeValues.data(),
                                               probeKeys.size()) /
                  sizeof(typename decltype(output)::value_type));
    return output;
  }

  template <typename NextHashTable> auto chain(NextHashTable& nextTable) {
    return ProbeChain(*this, std::make_unique<ProbeChain<NextHashTable>>(nextTable));
  }

  HashTableMonad(Configuration c)
      : buffer(c.size),
        internal{
            [this, c]() -> Internals {
              namespace cg = codegen;
              auto builder = cg::module_builder(compiler, "module_name");
              auto aggregate =
                  std::map<Aggregation, std::function<codegen::value<Value>(
                                            codegen::value<Value>, codegen::value<Value>)>>{
                      {Aggregation::Sum,
                       [](auto first, auto second) {
                         return codegen::value<Value>{(first + second).eval(), ""};
                       }},
                      {Aggregation::None, [](auto, auto second) { return second; }}}
                      .at(c.aggregation);

              auto hashBuildFunction =
                  builder.create_function<void(char*, unsigned int, Key, Value)>(
                      "function_name",
                      [&](cg::value<char*> state, cg::value<unsigned int> hashTableSize,
                          cg::value<Key> key, cg::value<Value> v) {
                        auto crc = calculateCRC(builder, key);
                        auto slotPosition = cg::variable<unsigned int>("slot", crc % hashTableSize);
                        Slot slot{state, slotPosition};
                        cg::while_(
                            [&] {
                              return cg::value<bool>{
                                  c.aggregation == Aggregation::None
                                      ? (cg::load((slot.markerAddress())) !=
                                         cg::constant<decltype(Entry::marker)>(0))
                                            .eval()
                                      : ((cg::load((slot.keyAddress())) != key) &
                                         (cg::load((slot.markerAddress())) !=
                                          cg::constant<decltype(Entry::marker)>(0)))
                                            .eval(),
                                  ""};
                            },
                            [&] { slot.set((slot.get() + cg::constant(1u)) % hashTableSize); });
                        cg::store(aggregate(cg::load(slot.valueAddress()), v), slot.valueAddress());
                        cg::store(key, slot.keyAddress());
                        cg::store(cg::constant<decltype(Entry::marker)>(1), slot.markerAddress());
                        cg::return_();
                      });

              auto bulkHashBuildFunction =
                  builder.create_function<void(char*, unsigned int, Key*, Value*, unsigned int)>(
                      "bulk_insert",
                      [&](cg::value<char*> state, cg::value<unsigned int> hashTableSize,
                          cg::value<Key*> keys, cg::value<Value*> values,
                          cg::value<unsigned int> inputSize) {
                        auto iterator =
                            cg::variable<unsigned>("iterator", cg::constant<unsigned>(0));
                        cg::while_([&] { return iterator.get() < inputSize; },
                                   [&] {
                                     auto key = cg::load(cg::bit_cast<Key*>(keys + iterator.get()));
                                     auto value =
                                         cg::load(cg::bit_cast<Value*>(values + iterator.get()));
                                     cg::call(hashBuildFunction, state, hashTableSize, key, value);
                                     iterator.set(iterator.get() + cg::constant(1u));
                                   });
                        cg::return_();
                      });
              auto m = std::move(builder).build();
              auto bulkInsertFunction = m.get_address(bulkHashBuildFunction);
              auto insertFunction = m.get_address(hashBuildFunction);
              return {std::move(m), insertFunction, bulkInsertFunction};
            }()} {};
};
