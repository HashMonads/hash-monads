#include "HashTableMonad.hpp"
#include <algorithm>
#include <benchmark/benchmark.h>
#include <random>
#include <tuple>
#include <unordered_map>
#include <vector>

using namespace std;

static void PlainHashBuild(benchmark::State& state) {
  vector<int> input;
  input.reserve(state.range(0));

  for(auto i = 0ll; i < state.range(0); i++)
    input.push_back(i);

  std::shuffle(input.begin(), input.end(), mt19937(random_device()()));

  for(auto _ : state) {
    HashTableMonad<int, int> table({.size = state.range(0) * 2ul});
    for(auto i = 0ll; i < state.range(0); i++)
      table.insert({input[i], i});
  }
}
BENCHMARK(PlainHashBuild)->Range(4, 1024 * 1024 * 16);

auto getInputData(benchmark::State& state) {
  vector<int> build;
  vector<int> buildPositions;
  build.reserve(state.range(0));
  buildPositions.reserve(state.range(0));
  vector<int> probe;
  vector<int> probePositions;
  probe.reserve(state.range(1));
  probePositions.reserve(state.range(1));

  for(auto i = 0ll; i < state.range(0); i++) {
    build.push_back(i);
    buildPositions.push_back(i);
  }
  for(auto i = 0ll; i < state.range(1); i++) {
    probe.push_back(i);
    probePositions.push_back(i);
  }
  std::shuffle(build.begin(), build.end(), mt19937(random_device()()));
  std::shuffle(probe.begin(), probe.end(), mt19937(random_device()()));
  return std::tuple{build, buildPositions, probe, probePositions};
}

static void STLHashJoin(benchmark::State& state) {
  auto [build, buildPositions, probe, probePositions] = getInputData(state);
  for(auto _ : state) {
    std::unordered_map<int, int> hashTable;
    for(size_t i = 0; i < build.size(); i++)
      hashTable[build[i]] = buildPositions[i];

    std::vector<std::tuple<int, int>> output;
    for(size_t i = 0; i < probe.size(); i++) {
      if(hashTable.count(probe[i]))
        output.push_back({hashTable[probe[i]], probePositions[i]});
    }

    benchmark::DoNotOptimize(output.size());
  }
}
BENCHMARK(STLHashJoin)
    ->Ranges({{128 * 1024, 16 * 1024 * 1024}, {4 * 1024 * 1024, 128 * 1024 * 1024}});

static void MonadHashJoin(benchmark::State& state) {
  auto [build, buildPositions, probe, probePositions] = getInputData(state);

  for(auto _ : state) {
    HashTableMonad<int, int> table({.size = build.size() * 2ul});
    table.insert(build, buildPositions);
    auto output = table.at(probe, probePositions);
    benchmark::DoNotOptimize(output.size());
  }
}
BENCHMARK(MonadHashJoin)
    ->Ranges({{128 * 1024, 16 * 1024 * 1024}, {4 * 1024 * 1024, 128 * 1024 * 1024}});

auto getThreeWayInputData(benchmark::State& state) {
  vector<int> build;
  vector<int> buildPositions;
  build.reserve(state.range(0));
  buildPositions.reserve(state.range(0));
  vector<int> build2;
  vector<int> buildPositions2;
  build2.reserve(state.range(0));
  buildPositions2.reserve(state.range(0));
  vector<int> probe;
  vector<int> probePositions;
  probe.reserve(state.range(1));
  probePositions.reserve(state.range(1));

  for(auto i = 0ll; i < state.range(0); i++) {
    build.push_back(i);
    buildPositions.push_back(i);
    build2.push_back(i);
    buildPositions2.push_back(i);
  }
  for(auto i = 0ll; i < state.range(1); i++) {
    probe.push_back(i);
    probePositions.push_back(i);
  }
  std::shuffle(build.begin(), build.end(), mt19937(random_device()()));
  std::shuffle(build2.begin(), build2.end(), mt19937(random_device()()));
  std::shuffle(probe.begin(), probe.end(), mt19937(random_device()()));
  return std::tuple{build, buildPositions, build2, buildPositions2, probe, probePositions};
}

static void ThreeWaySTLHashJoin(benchmark::State& state) {
  auto [build, buildPositions, build2, buildPositions2, probe, probePositions] =
      getThreeWayInputData(state);
  for(auto _ : state) {
    std::unordered_map<int, int> hashTable;
    for(size_t i = 0; i < build.size(); i++)
      hashTable[build[i]] = buildPositions[i];
    std::unordered_map<int, int> hashTable2;
    for(size_t i = 0; i < build2.size(); i++)
      hashTable[build2[i]] = buildPositions2[i];

    std::vector<std::tuple<int, int, int>> output;
    for(size_t i = 0; i < probe.size(); i++) {
      if(hashTable.count(probe[i]))
        output.push_back({probe[i], hashTable[probe[i]], probePositions[i]});
    }

    std::vector<std::tuple<int, int, int>> finalOutput;
    for(size_t i = 0; i < output.size(); i++) {
      if(hashTable2.count(std::get<0>(output[i])))
        finalOutput.push_back(
            {std::get<1>(output[i]), std::get<2>(output[i]), hashTable2[std::get<0>(output[i])]});
    }

    benchmark::DoNotOptimize(finalOutput.size());
  }
}
BENCHMARK(ThreeWaySTLHashJoin)
    ->Ranges({{128 * 1024, 16 * 1024 * 1024}, {4 * 1024 * 1024, 128 * 1024 * 1024}});

static void ThreeWayMonadHashJoin(benchmark::State& state) {
  auto [build, buildPositions, build2, buildPositions2, probe, probePositions] =
      getThreeWayInputData(state);

  for(auto _ : state) {
    HashTableMonad<int, int> table({.size = build.size() * 2ul});
    table.insert(build, buildPositions);
    HashTableMonad<int, int> table2({.size = build2.size() * 2ul});
    table2.insert(build2, buildPositions2);
    auto output = table.chain(table2).at<int, int, int>(probe, probePositions);
    benchmark::DoNotOptimize(output.size());
  }
}
BENCHMARK(ThreeWayMonadHashJoin)
    ->Ranges({{128 * 1024, 8 * 1024 * 1024}, {4 * 1024 * 1024, 64 * 1024 * 1024}});

BENCHMARK_MAIN();
