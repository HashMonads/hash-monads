#ifndef PROBECHAIN_H
#define PROBECHAIN_H
#include <optional>
#include <tuple>
#include <vector>
#define LLVM_DISABLE_ABI_BREAKING_CHECKS_ENFORCING 1
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#include <codegen/arithmetic_ops.hpp>
#include <codegen/compiler.hpp>
#include <codegen/module.hpp>
#include <codegen/relational_ops.hpp>
#include <codegen/statements.hpp>
#include <codegen/variable.hpp>
#pragma clang diagnostic pop
#include "Probe.hpp"
#include <iostream>
#include <memory>

template <typename T> struct tag { using type = T; };

template <typename KeyType> struct EndOfProbeChain {
  using HashTableType = char;
  using BuiltProbeFunction = codegen::function_ref<unsigned int, char*, unsigned int, int>;

  template <typename Ignored>
  std::function<void(codegen::module_builder& builder)> getBuildProbeFunction() {
    return [](auto&) {};
  }
  std::function<BuiltProbeFunction()> getProbeFunction() {
    throw std::logic_error("end of probe chain should not be asked for a probe function");
  };

  void fillBufferBuffer(char**) {}
  void fillSizeBuffer(unsigned int*) {}

  auto getProbeGeneratorFunction() {
    namespace cg = codegen;
    return [](codegen::value<char**> /*pointerToHashTable*/,
                  cg::value<unsigned int*> /*hashTableSizesInEntries*/,
                  codegen::variable<unsigned int>& /*positionOfPotentialMatchInCurrentHashTable*/,
                  cg::value<KeyType> /*probeKey*/, codegen::variable<char*>& /*outputColumnOffset*/,
                  std::function<void(codegen::variable<char*>&)> /*nextProbe*/) {};
  }
};

template <typename HashTable, typename RestOfChain = EndOfProbeChain<typename HashTable::KeyType>>
class ProbeChain {
  HashTable& firstHashTable;
  std::unique_ptr<RestOfChain> chain;
  codegen::compiler compiler;

public:
  ProbeChain(HashTable& firstHashTable) : firstHashTable(firstHashTable){};
  ProbeChain(HashTable& firstHashTable, std::unique_ptr<RestOfChain>&& chain)
      : firstHashTable(firstHashTable), chain(std::move(chain)){};
  ProbeChain(ProbeChain const&) = default;
  ProbeChain(ProbeChain&&) = default;
  using HashTableType = HashTable;

  void fillBufferBuffer(char** buffer) {
    *buffer = (char*)firstHashTable.getBuffer().data();
    if(chain)
      chain->fillBufferBuffer(&(buffer[1]));
  };

  void fillSizeBuffer(unsigned int* buffer) {
    *buffer = (unsigned int)firstHashTable.getBuffer().size();
    if(chain)
      chain->fillSizeBuffer(&(buffer[1]));
  };

  template <typename... ResultColumnTypes>
  auto at(std::vector<typename HashTable::KeyType> probeKeys,
          std::vector<typename decltype((tag<ResultColumnTypes>{}, ...))::type> probeValues,
          std::optional<size_t> providedSizeEstimate = {}) {
    std::vector<std::tuple<ResultColumnTypes...>> result;
    result.resize(providedSizeEstimate.value_or(probeKeys.size() * 2));
    using InstantiatedProbe = Probe<HashTable, typename decltype(probeValues)::value_type,
                                    std::tuple<ResultColumnTypes...>>;
    auto probe =
        chain ? InstantiatedProbe(compiler, getBuildProbeFunction<InstantiatedProbe>(),
                                  getProbeFunction(), getProbeGeneratorFunction())
              : InstantiatedProbe(compiler, getBuildProbeFunction<InstantiatedProbe>(),
                                  getProbeFunction(), InstantiatedProbe::processSlotImplementation);
    char* hashTableBuffers[255]{};
    fillBufferBuffer(hashTableBuffers);
    unsigned int hashTableSizes[255]{};
    fillSizeBuffer(hashTableSizes);
    auto actualSizeInBytes = probe.internals.performProbe(
        (char*)result.data(), result.size(), (char**)hashTableBuffers, hashTableSizes,
        probeKeys.data(), probeValues.data(), probeKeys.size());
    result.resize(actualSizeInBytes / sizeof(typename decltype(result)::value_type));

    return result;
  }

  using BuiltProbeFunction =
      codegen::function_ref<unsigned int, char*, unsigned int, typename HashTable::KeyType>;
  BuiltProbeFunction builtProbeFunction{"empty", nullptr};

  template <typename InstantiatedProbe>
  std::function<void(codegen::module_builder& builder)> getBuildProbeFunction() {
    return [this](codegen::module_builder& builder) {
      InstantiatedProbe::prepareFindCandidateSlotFunctionImplementation(builder);
      builtProbeFunction = InstantiatedProbe::createFindCandidateSlotFunctionImplementation();
      if(chain) // recurse
        chain->template getBuildProbeFunction<
            Probe<typename RestOfChain::HashTableType, int, std::tuple<int>>>()(builder);
    };
  }

  std::function<BuiltProbeFunction()> getProbeFunction() {
    return [this]() -> BuiltProbeFunction {
      assert(builtProbeFunction.operator llvm::Function*());
      return builtProbeFunction;
    };
  }

  /**
   * This function generates the next step in the probing process
   */
  auto getProbeGeneratorFunction() {
    namespace cg = codegen;
    return [this](codegen::value<char**> pointerToHashTable,
                  cg::value<unsigned int*> hashTableSizesInEntries,
                  codegen::variable<unsigned int>& positionOfPotentialMatchInCurrentHashTable,
                  cg::value<typename HashTable::KeyType> probeKey,
                  codegen::variable<char*>& outputColumnOffset,
                  std::function<void(codegen::variable<char*>&)> processProbePayload) {
      typename HashTable::Slot slot{cg::load(pointerToHashTable),
                                    positionOfPotentialMatchInCurrentHashTable};
      cg::if_(cg::load(slot.markerAddress()) > cg::constant(0), [&] {
        cg::store(cg::load(slot.valueAddress()),
                  cg::bit_cast<typename HashTable::ValueType*>(outputColumnOffset.get()));
        outputColumnOffset.set(outputColumnOffset.get() +
                               cg::constant(sizeof(typename HashTable::ValueType)));
        if(chain) {
          auto findCandidateSlot = chain->getProbeFunction()();
          codegen::value<char**> pointerToNextHashTable{
              (pointerToHashTable + cg::constant(1)).eval(), ""};
          codegen::value<unsigned int*> nextHashTableSize{
              (hashTableSizesInEntries + cg::constant(1)).eval(), ""};

          auto hashTableSlotPosition = cg::variable<unsigned int>(
              "candidateSlot", cg::call(findCandidateSlot, cg::load(pointerToNextHashTable),
                                        cg::load(nextHashTableSize), probeKey));
          chain->getProbeGeneratorFunction()(pointerToNextHashTable, nextHashTableSize,
                                             hashTableSlotPosition, probeKey, outputColumnOffset,
                                             processProbePayload);
        } else {
          processProbePayload(outputColumnOffset);
        }
      });
    };
  }
};

#endif /* PROBECHAIN_H */
